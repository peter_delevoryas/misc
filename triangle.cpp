#include <iostream>
#include <algorithm>    // std::sort
#include <cmath>        // pow, fabs
#include <array>        // std::array
#include <cstdlib>      // strtod

class Triangle {
private:
    static constexpr double TOLERANCE = 0.0001;
    double a, b, c;
public:
    Triangle(const double &x, const double &y, const double &z) noexcept;
    inline bool is_right() noexcept {
        if (fabs(pow(a, 2) + pow(b, 2) - pow(c, 2)) <= TOLERANCE)
            return true;
        else
            return false;
    }
    inline bool is_acute() noexcept {
        if (pow(a, 2) + pow(b, 2) - pow(c, 2) > TOLERANCE)
            return true;
        else
            return false;
    }
    inline bool is_obtuse() noexcept {
        if (is_right())
            return false;
        else if (is_acute())
            return false;
        else
            return true;
    }
    inline void print_type() noexcept {
        if (is_right())
            std::cout << "Right triangle" << std::endl;
        else if (is_acute())
            std::cout << "Acute triangle" << std::endl;
        else
            std::cout << "Obtuse triangle" << std::endl;
    }
};

Triangle::Triangle(const double &x, const double &y, const double &z) noexcept {
    std::array<double, 3> sides = { x, y, z };
    std::sort(sides.begin(), sides.end());
    a = sides.at(0);
    b = sides.at(1);
    c = sides.at(2);
}

int main() {
    std::array<double, 3> tmp;

    // Get input
    for (int i = 0; i < 3; ++i) {
        std::string input;
        std::cout << "Enter side " << i + 1 << std::endl;
        std::getline(std::cin, input);
        double d = strtod(input.c_str(), nullptr);
        if (d == 0.0) {
            std::cout << "Error, invalid side argument" << std::endl;
            --i;
            continue;
        }
        tmp.at(i) = d;
    }

    Triangle t(tmp.at(0), tmp.at(1), tmp.at(2));
    t.print_type();
}
