#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdio>

#define loop while(1)
#define EVENT_FILE_NAME "events.txt"

struct Event {
    std::string title;
    std::string desc;
    std::string time;
    void write_to_file(std::string filename) {
        std::ofstream file(filename, std::ios::out|std::ios::app);
        file << std::endl;
        file << title << std::endl;
        file << desc << std::endl;
        file << time << std::endl;
        file.close();
    }
};

void new_event() {
    Event event;
    printf("Enter the event's name:\n");
    std::string input;
    if (!std::getline(std::cin, input)) {
        printf("aborting new_event()...\n");
        return;
    }
    event.title = input;
    printf("Now enter a description of the event:\n");
    while (std::getline(std::cin, input)) {
        if (input == "")
            break;
        event.desc += input;
    }
    printf("Now enter the date this event must be completed by:\n");
    if (!std::getline(std::cin, input)) {
        printf("aborting new_event()...\n");
        return;
    }
    event.time = input;

    // Write event to file
    printf("Writing event to file\n");
    event.write_to_file(EVENT_FILE_NAME);
}

void print_events() {
    
}

int main(int argc, char *argv[]) {
    loop {
        printf("1. New event\n");
        printf("2. Print events\n");
        std::string input;
        if (!std::getline(std::cin, input)) {
            printf("have a nice day!\n");
            break;
        }
        printf("received: %s\n", input.c_str());
        switch (input[0]) {
        case '1':
            new_event();
            break;
        case '2':
            print_events();
            break;
        default:
            printf("error, enter 1 or 2\n");
            break;
        }
    }
}
