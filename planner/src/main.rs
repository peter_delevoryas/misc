use std::io;

fn main() {
    println!("New event title:");
    let mut title = String::new();
    io::stdin().read_line(&mut title)
        .ok()
        .expect("Failed to read line");
    println!("Received: {}", &title[..(title.len()-1)]);
    println!("Enter event description:");
}
